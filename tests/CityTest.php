<?php

use \PHPUnit\Framework\TestCase;

include 'City.php';


class CityTest extends TestCase
{

    private $city;

    protected function setUp(): void
    {
        $this->city = new City();
    }

    /**
     * @dataProvider addDataProvider
     */
    public function testAddress($street, $expected){
        $result = $this->city->getAddressesByStreet($street);

        $this->assertEquals($expected, $result);

    }


    public function addDataProvider() {
        return array(
            array('Centralna', 1),
            array('Centralna', 1),

);
}
protected function tearDown(): void
{
    isset($this->city);
}
}