<?php


class Address
{
 private $number;
 private $street;
 private $index;

    /**
     * Address constructor.
     * @param $number
     * @param $street
     * @param $index
     */
    public function __construct($number, $street, $index)
    {
        $this->number = $number;
        $this->street = $street;
        $this->index = $index;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param mixed $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

}