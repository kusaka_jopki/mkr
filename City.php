<?php

include 'Address.php';

class City
{
private $addresses;
public function __construct()
{
    $this->addresses=array(
        new Address('15', 'Bazhana', '07500'),
        new Address('49', 'Centralna', '07501'),
        new Address('45', 'Pryvokzalna', '07500'),
    );
}

public function addAddress($number, $street, $index){
    $this->addresses[] = new Address($number, $street, $index);
}


public function getAddressesCountByIndex($index) {
    $count = 0;

    foreach ($this->addresses as $address){
        if($address->getIndex() == $index){
    $count++;
        }
    }
    return $count;
}
public function getAddressesByStreet($street){
    $count = 0;

    foreach ($this->addresses as $address){
        if($address->getIndex() == $street){
            $count++;
        }
    }
    return $count;
}
}